This repository contains code that symbolically verifies the irreducibility conditions in the paper 'Speeding up Langevin Dynamics by Mixing'.
